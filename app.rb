require 'rubygems'
require 'sinatra'
require 'sinatra/reloader'
require 'sinatra/activerecord'

# set :database, "sqlite3:clinica.db"

class Patient < ActiveRecord::Base
	validates :name, presence: true, length: { minimum: 3 }
	validates :phone, :doctor, :datestamp, presence: true
end

class Doctor < ActiveRecord::Base
end

before do
  @doctors = Doctor.all
end

get '/' do
  erb :index
end

get '/visit' do
  @patient = Patient.new
  erb :visit
end

post '/visit' do
  @patient = Patient.new params[:patient]
  if @patient.save
    erb "<h2>Thank you! You made an appointment with the doctor</h2>"
  else
    @error = @patient.errors.full_messages.first
    erb :visit
  end

end

get '/doctor/:id' do
  @doctor = Doctor.find(params[:id])
  erb :doctor
end

get '/bookings' do
  @patients = Patient.order('created_at DESC')
  erb :bookings
end

get '/patient/:id' do
  @patient = Patient.find(params[:id])
  erb :patient
end
