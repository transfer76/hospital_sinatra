class CreateDoctors < ActiveRecord::Migration[5.2]
  def change
  	create_table :doctors do |t|
  		t.string :name
  		t.string :specialty

  		t.timestamps
  	end
	  Doctor.create :name => 'Pupkin Petr', :specialty => 'therapist'
	  Doctor.create :name => 'Ivanova Vasya', :specialty => 'ENT doctor'
	  Doctor.create :name => 'Petrov Sveta', :specialty => 'immunologist'

  end
end
